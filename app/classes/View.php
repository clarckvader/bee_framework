<?php

class View {
    public static function render($view, $data = []){
        //Convertir el array asociativo en objeto
        $d = to_object($data); //Data en array o d para objetos del tipo ($obj->elmnt)

        if(!is_file(VIEWS.CONTROLLER.DS.$view.'View.php')){
            die(printf('No existe la vista %sView en el directorio %s', $view, CONTROLLER));
        }
        require_once VIEWS.CONTROLLER.DS.$view.'View.php';
        exit();
    }
}