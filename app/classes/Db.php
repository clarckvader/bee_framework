<?php

class Db
{
    private $link;
    private $engine;
    private $host;
    private $name;
    private $user;
    private $pass;
    private $charset;

    function __concstruct()
    {
        /**
         * Constructor para nuestra clase
         * 
         */
        $this->engine  =  IS_LOCAL ?   LDB_ENGINE : DB_ENGINE;
        $this->host    =  IS_LOCAL ?     LDB_HOST : DB_HOST;
        $this->name    =  IS_LOCAL ?     LDB_NAME : DB_NAME;
        $this->user    =  IS_LOCAL ?     LDB_USER : DB_USER;
        $this->pass    =  IS_LOCAL ?     LDB_PASS : DB_PASS;
        $this->charset =  IS_LOCAL ?  LDB_CHARSET : DB_CHARSET;


        
        return $this;
    }
    /**
     * Metodo para abrir una conexion a la base de datos
     * 
     */
     private function connect()
    {
        try
        {
                $this->link = new PDO(LDB_ENGINE.':host='.LDB_HOST.';dbname='.LDB_NAME.';charset='.LDB_CHARSET , LDB_USER, LDB_PASS);
                return $this->link;
        }
        catch(PDOException $e)
        {
            die(sprintf('No hay conexion a la base de datos, hubo un error: %s', $e->getMessage()));
        }
    }

    /**
     * Metodo para hacer un query a la base datos
     * 
     * @param string $sql
     * @param array $param
     * @return void;
     */

     public static function query($sql, $params = [])
     {
        $db = new self();
        $link = $db->connect(); //conexion a la base de datos
        $link->beginTransaction(); //punto de guardado para regresar ante algun error
        $query = $link->prepare($sql);

        //Menejando errores en el query a la peticion
        if(!$query->execute($params))
        {
            $link->rollBack();
            $error = $query->errorInfo();
            throw new Exception($error[2]);
        }
        if(strpos($sql,'SELECT') !==false) //False si no hay filas
        {
            return $query->rowCount()>0 ? $query->fetchAll() : false;
        
        }elseif(strpos($sql,'INSERT') !==false)
        {
            $id = $link->lastInsertId();
            $link->commit();
            return $id;
        }elseif(strpos($sql,'UPDATE') !==false)
        {
            $link->commit();
            return true;
        }elseif(strpos($sql,'DELETE') !==false)
        {
            if($query->rowCount()>0)
            {
                $link->commit();
                return true;
            }

            $link->rollBack();
            return false; //Nada ha sido borrado
        }else
        {
            $link->commit();
            return true;
        }
     }
}




