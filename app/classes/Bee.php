<?php

class Bee {

    private $framework = 'Bee Framework';
    private $version = '1.0.0';
    private $uri = [];


    function __construct()
    {
        $this->init();

    }
    /**
     * Metodo para ejecutar cada metodo de forma subsecuente
     * @return void
     */
    private function init(){
        $this->init_session();
        $this->init_load_config();
        $this->init_load_functions();
        $this->init_autoload();
        $this->dispatch();
    }

    /**
     * Metodo para iniciar la sesion en le sistema
     * @return void
     */

     private function init_session(){
         if(session_status() == PHP_SESSION_NONE){
             session_start();
         }
     }

     /**
      * metodo para cargar la configuracion del sistema
      *@return void
      */
     private function init_load_config(){
         $file = 'bee_config.php';
         if(!is_file('app/config/'.$file)){
             die(sprintf('El archivo %s no se encuentra, es requerido para que %s funcione', $file, $this->framework));
         }
         require_once 'app/config/'.$file;
         return;
     }

     /**
      * Metodo para cargar las funciones del sistema y del usuario
      *@return void
      */
    private function   init_load_functions(){
        $file ='bee_core_functions.php';
         if(!is_file(FUNCTIONS.$file)){
             die(sprintf('El archivo %s no se encuentra, es requerido para que %s funcione', $file, $this->framework));
         }
         //Cargando core functions
         require_once FUNCTIONS.$file;
         
         $file ='bee_custom_functions.php';
         if(!is_file(FUNCTIONS.$file)){
             die(sprintf('El archivo %s no se encuentra, es requerido para que %s funcione', $file, $this->framework));
         }
         require_once FUNCTIONS.$file;
         return;
    }
    /**
     * Metodo para cargar todos los archivos de forma automatica
     *@return void 
     */
    private function init_autoload(){
        //require_once CLASSES.'Db.php';
        //require_once CLASSES.'Model.php';
        //require_once CLASSES.'Controller.php';
        //require_once CLASSES.'View.php';
        require_once CLASSES.'Autoloader.php';
        //require_once CONTROLLERS.DEFAULT_CONTROLLER.'Controller.php';
        //require_once CONTROLLERS.DEFAULT_ERROR_CONTROLLER.'Controller.php';
        Autoloader::init();
        return;
    }
    
    /**
     * Metodo para filtrar, descomponer y sanitizar los elementos de nuestra uri
     * @return void;
     */
    private function  filter_url(){
        if(isset($_GET['uri'])){
            $this->uri = $_GET['uri'];
            $this->uri = rtrim($this->uri,'/');
            $this->uri = filter_var($this->uri, FILTER_SANITIZE_URL);
            $this->uri = explode('/', strtolower($this->uri));
            return $this->uri;
        }
    }

    /**
     *Metodo para cargar y ejecutar automaticamente el controlador soliciatado por el usuario,
     *sus metodos y parametros
     * @return void
     */
    private function dispatch(){
        //Filtrar url y separar la uri
        $this->filter_url();

        //////////////////////////////////////////////////////////
        //Saber si se esta pasando un parametro por URI
        if(isset($this->uri[0])){
            $current_controller = $this->uri[0];
            unset($this->uri[0]);
        }else{
            $current_controller = DEFAULT_CONTROLLER;
        }
        
        $controller = $current_controller.'Controller';
        if(!class_exists($controller)){
            $current_controller = DEFAULT_ERROR_CONTROLLER;
            $controller = DEFAULT_ERROR_CONTROLLER.'Controller';//ErrorController
        }

        ///////////////////////////////////////
        // Ejecucion del metodo solicitado
        if(isset($this->uri[1])){
            $method = str_replace('-','_',$this->uri[1]);
            
            //Comprobar si existe el metodo de la clase a ejecutar(controlador);
            if(!method_exists($controller, $method)){
                $controller = DEFAULT_ERROR_CONTROLLER.'Controller';
                $current_method = DEFAULT_METHOD; //index
                $current_controller = DEFAULT_ERROR_CONTROLLER;
            }else{
                $current_method = $method; 
            }

            unset($this->uri[1]);
        }else{
            $current_method = DEFAULT_METHOD; //index
        }
        //////////////////////////////////////////////////
        //Creando constantes para utilizarlas mas tarde
        define('CONTROLLER', $current_controller);
        define('METHOD', $current_method);

        ///////////////////////////////////////
        // Ejecutando controlador y metodo segun se haga la
        //peticion
        $controller = new $controller;

        $params = array_values(empty($this->uri) ? [] : $this->uri);
        //Llamada al metodo solicitado por el usuario en curso
        if(empty($params)){
            call_user_func([$controller, $current_method]);
        }else{
            call_user_func_array([$controller, $current_method], $params);
        }
        return;
    }
        /**
     * Correr el framework
     * @return void
     */

    public static function fly(){
        $bee = new self();
        return;
    }
};

