<?php

//Saber si estamos trabajando en local o remoto

define('IS_LOCAL'                ,in_array($_SERVER['REMOTE_ADDR'], ['127.0.0.1','::1']));

//Definir el uso de horario(Time Zone)
date_default_timezone_set('America/La_Paz');

//Lenguaje

define('LANG'                    ,'es');

define('BASEPATH'                , IS_LOCAL ? '/bee_framework/':'_____Basepath_Produccion______');

//Sal del sistema

define('AUTH_SALT'               ,'Passauthsecret');

//puerto

define('PORT'                    ,'8848');

define('URL'                     , IS_LOCAL ? 'http://127.0.0.1:'.PORT.'/bee_framework/':'_____Basepath_Produccion______');

//RUtas de nuestro sistema y archivo

define('DS'                      , DIRECTORY_SEPARATOR);
define('ROOT'                    , getcwd().DS);

define('APP'                     , ROOT.'app'.DS);

define('CLASSES'                 , APP.'classes'.DS);
define('CONFIG'                  , APP.'config'.DS);
define('CONTROLLERS'             , APP.'controllers'.DS);
define('FUNCTIONS'               , APP.'functions'.DS);
define('MODELS'                  , APP.'models'.DS);

define('TEMPLATES'               ,ROOT.'templates'.DS);
define('INCLUDES'                ,TEMPLATES.'includes'.DS);
define('MODULES'                 ,TEMPLATES.'modules'.DS);
define('VIEWS'                   ,TEMPLATES.'views'.DS);

define('ASSETS'                  ,URL.'assets/');
define('CSS'                     ,ASSETS.'css/');
define('FAVICON'                 ,ASSETS.'favicon/');
define('FONTS'                   ,ASSETS.'fonts/');
define('IMAGES'                  ,ASSETS.'images/');
define('JS'                      ,ASSETS.'js/');
define('PLUGINS'                 ,ASSETS.'plugins/');
define('UPLOADS'                 ,ASSETS.'images/');

//Credenciales base de datos

//Set para conexion local 
define('LDB_ENGINE'              ,'mysql');
define('LDB_HOST'                ,'localhost');
define('LDB_NAME'                ,'beefm');
define('LDB_USER'                ,'roberto');
define('LDB_PASS'                ,'manuel955');
define('LDB_CHARSET'             ,'utf8');

//Set para conexion remota
define('DB_ENGINE'               ,'mysql');
define('DB_HOST'                 ,'localhost');
define('DB_NAME'                 ,'___REMOTE DB___');
define('DB_USER'                 ,'___REMOTE DB___');
define('DB_PASS'                 ,'___REMOTE DB___');
define('DB_CHARSET'              ,'___REMOTE DB___');

//Controlador por defecto

define('DEFAULT_CONTROLLER'      , 'home');
//Metodo por defecto
define('DEFAULT_METHOD'          , 'index');
//Controlador de errores
define('DEFAULT_ERROR_CONTROLLER', 'error');